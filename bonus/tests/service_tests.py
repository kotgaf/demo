import pytest

from django.conf import settings
from django.core.exceptions import ValidationError

from .factories import BonusAccountFactory
from apps.bonus.services import BonusService


@pytest.mark.django_db
class TestBonusService:
    def test_add_bonus(self):
        user_id = 42
        value = 999
        comment = 'test#'
        BonusAccountFactory(balance=0, user_id=42)
        assert BonusService.add_bonus(value=value, user_id=user_id, comment=comment) == value

    def test_add_bonus_negative(self):
        user_id = 42
        value = -999
        comment = 'test#'
        BonusAccountFactory(balance=0, user_id=42)
        with pytest.raises(ValidationError):
            BonusService.add_bonus(value=value, user_id=user_id, comment=comment) == value

    def test_add_offer_bonus(self):
        sum = 27880
        user_id = 42
        bonus = int(round(sum * settings.OFFER_BONUS_RATE))
        BonusService.add_bonus_for_offer(offer_sum=sum, user_id=user_id)
        balance = BonusService.user_balance(user_id=user_id)
        assert balance == bonus

    def test_user_balance(self):
        user_id = 7
        value_1 = 100
        value_2 = 200
        BonusService.add_bonus(value=value_1, user_id=user_id)
        assert BonusService.user_balance(user_id=user_id) == value_1
        BonusService.add_bonus(value=value_2, user_id=user_id)
        assert BonusService.user_balance(user_id=user_id) == value_1 + value_2

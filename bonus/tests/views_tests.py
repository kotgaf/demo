import pytest
import ujson

from django.test import RequestFactory
from django.urls import reverse

from apps.bonus.views import BonusView
from apps.bonus.tests.factories import BonusAccountFactory, BonusTransactionFactory
from apps.bonus.tests.mocks import MockedUser, BonusServiceMock, BonusViewMock


@pytest.mark.django_db
class TestBonusView:
    def test_get_bonus_balance(self, mocker):
        user_id = 7
        balance = 1500
        account = BonusAccountFactory(balance=balance, user_id=user_id)
        user = MockedUser(user_id=user_id)
        request = RequestFactory().get(reverse('bonus'))
        request.user = user
        BonusViewMock.mock_auth_mixin_for_get_balance(mocker, BonusView, request)
        response = BonusView.as_view()(request)
        content = ujson.loads(response.content)
        assert response.status_code == 200
        assert content.get('balance') == balance

    def test_get_bonus_balance_with_mock(self, mocker):
        user_id = 7
        balance = 1500
        BonusServiceMock.mock_get_balance(mocker=mocker, balance=balance)
        user = MockedUser(user_id=user_id)
        request = RequestFactory().get(reverse('bonus'))
        request.user = user
        BonusViewMock.mock_auth_mixin_for_get_balance(mocker, BonusView, request)
        response = BonusView.as_view()(request)
        content = ujson.loads(response.content)
        assert response.status_code == 200
        assert content.get('balance') == balance

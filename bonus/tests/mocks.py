from apps.user.auth_backend import OnlyAdminCanChangeMixin, TokenLoginRequiredMixin


class MockedUser:
    def __init__(self, **kwargs):
        self.id = kwargs.get('user_id')


class BonusViewMock:
    @staticmethod
    def mock_auth_mixin_for_get_balance(mocker, view, request):
        mocker.patch('apps.user.auth_backend.TokenLoginRequiredMixin.dispatch')
        TokenLoginRequiredMixin.dispatch.return_value = view.get(view, request)


class BonusServiceMock:
    @staticmethod
    def mock_get_balance(mocker, balance=0):
        mocker.patch('apps.bonus.services.BonusService.user_balance', return_value=balance)

import random
import factory

from factory.django import DjangoModelFactory

from apps.bonus.models import BonusAccount, BonusTransaction


class BonusAccountFactory(DjangoModelFactory):
    class Meta:
        model = BonusAccount

    user_id = factory.sequence(lambda n: n)
    balance = 0


class BonusTransactionFactory(DjangoModelFactory):
    class Meta:
        model = BonusTransaction

    id = factory.sequence(lambda n: n)
    value = random.random() * 100
    account = factory.SubFactory(BonusAccountFactory)
    comment = factory.sequence(lambda n: 'comment%s' % n)

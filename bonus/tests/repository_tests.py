import pytest

from apps.bonus.models import BonusTransaction, BonusAccount
from apps.bonus.repository import BonusRepository
from apps.bonus.tests.factories import BonusAccountFactory, BonusTransactionFactory


@pytest.mark.django_db
class TestBonusRepository:
    def test_get_user_balance(self):
        user_id = 9919919
        assert BonusRepository.get_user_balance(user_id=user_id) == 0

    def test_get_user_balance_got_balance(self):
        account = BonusAccountFactory(balance=9999)
        assert BonusRepository.get_user_balance(user_id=account.user_id) == account.balance

    def test_update_bonus_balance(self):
        value_1 = -1987
        value_2 = 2000
        account = BonusAccountFactory()
        BonusTransactionFactory(account=account, value=value_1)
        BonusTransactionFactory(account=account, value=value_2)
        BonusRepository.update_bonus_balance(user_id=account.user_id)
        assert BonusRepository.get_user_balance(user_id=account.user_id) == value_1 + value_2

    def test_get_or_create_bonus_account(self):
        user_id = 1234
        account = BonusRepository.get_or_create_bonus_account(user_id=user_id)
        assert account.user_id == user_id

    def test_create_bonus_transaction(self):
        account = BonusAccountFactory()
        value = 1234
        return_value = BonusRepository.create_bonus_transaction(value=value, comment='', account=account)
        assert return_value == value


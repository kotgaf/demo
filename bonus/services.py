from django.core.exceptions import ValidationError
from django.conf import settings

from apps.bonus.repository import BonusRepository
from .forms import BonusAddForm


class BonusService:
    @classmethod
    def add_bonus_for_offer(cls, offer_sum, user_id, comment=None):
        value = round(offer_sum * settings.OFFER_BONUS_RATE)
        return cls.add_bonus(value=value, user_id=user_id, comment=comment)

    @classmethod
    def add_bonus(cls, value, user_id, comment=None):
        form = BonusAddForm({'value': value, 'user_id': user_id, 'comment': comment})
        if form.is_valid():
            return cls._modify_bonus(value=form.cleaned_data['value'],
                                     user_id=form.cleaned_data['user_id'],
                                     comment=form.cleaned_data['comment'])
        else:
            raise ValidationError(form.errors)

    @classmethod
    def user_balance(cls, user_id):
        return BonusRepository.get_user_balance(user_id=user_id)

    @classmethod
    def _modify_bonus(cls, value, user_id, comment=None):
        account = BonusRepository.get_or_create_bonus_account(user_id=user_id)
        BonusRepository.create_bonus_transaction(value=value, comment=comment, account=account)
        return BonusRepository.update_bonus_balance(user_id=user_id)

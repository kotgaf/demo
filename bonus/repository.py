from django.db.models import Sum

from .models import BonusAccount, BonusTransaction


class BonusRepository:
    @staticmethod
    def get_or_create_bonus_account(user_id):
        account, created = BonusAccount.objects.get_or_create(user_id=user_id)
        return account

    @staticmethod
    def create_bonus_transaction(value, comment, account):
        transaction = BonusTransaction.objects.create(value=value, comment=comment, account=account)
        return transaction.value

    @staticmethod
    def update_bonus_balance(user_id):
        account = BonusAccount.objects.get(user_id=user_id)
        transactions = BonusTransaction.objects.filter(account=account)
        if transactions:
            new_balance = transactions.aggregate(Sum('value'))['value__sum']
        else:
            new_balance = account.balance
        account.balance = new_balance
        account.save()
        return account.balance

    @classmethod
    def get_user_balance(cls, user_id):
        try:
            return BonusAccount.objects.get(user_id=user_id).balance
        except BonusAccount.DoesNotExist:
            return 0

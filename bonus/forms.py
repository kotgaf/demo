from django import forms


class BonusAddForm(forms.Form):
    def clean_value(self):
        data = int(self.cleaned_data['value'])
        if data < 0:
            raise forms.ValidationError('Добавление бонуса не должно быть отрицательным')
        return data

    value = forms.IntegerField()
    user_id = forms.IntegerField()
    comment = forms.CharField(max_length=100, required=False)

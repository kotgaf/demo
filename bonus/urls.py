from django.conf.urls import url
from django.views.decorators import csrf

from . import views

urlpatterns = [
    url('^$', csrf.csrf_exempt(views.BonusView.as_view()), name='bonus'),
]

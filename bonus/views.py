from django.views import View

from apps.user.auth_backend import TokenLoginRequiredMixin
from .services import BonusService
from utils.functions import ujson_response, ujson_response_with_error


class BonusView(TokenLoginRequiredMixin, View):
    def get(self, request):
        user_id = request.user.id
        try:
            response = {
                'code': '200',
                'status': 200,
                'json': {'balance': BonusService.user_balance(user_id=user_id)}
            }
        except BaseException as error:
            return ujson_response_with_error(error)
        return ujson_response(response)

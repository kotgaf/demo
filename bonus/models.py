from django.db import models

from utils.actual_model import ActualModel


class BonusAccount(ActualModel):
    user_id = models.IntegerField(verbose_name='ID пользователя', primary_key=True)
    balance = models.IntegerField(verbose_name='Бонусный счет пользователя', default=0)


class BonusTransaction(ActualModel):
    value = models.IntegerField(verbose_name='Значения изменения бонуса')
    comment = models.CharField(max_length=100, verbose_name='Комментарий', null=True, blank=True)
    account = models.ForeignKey(BonusAccount)
